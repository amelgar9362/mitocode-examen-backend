package com.mitocode.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.dto.SignosDTO;
import com.mitocode.model.Signos;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.ISignosVitalesRepo;
import com.mitocode.service.ISignosVitalesService;

@Service
public class SignosVitalesServiceImpl extends CRUDImpl<Signos, Integer> implements ISignosVitalesService {
	
	@Autowired
	private ISignosVitalesRepo repo;
	
	@Autowired
	private ModelMapper mapper;

	@Transactional
	@Override
	public Signos registrarTransaccional(SignosDTO signos) throws Exception {
		Signos map = mapper.map(signos, Signos.class);
		repo.save(map);
		return map;
	}

	@Override
	protected IGenericRepo<Signos, Integer> getRepo() {
		return repo;
	}

	@Override
	public Page<Signos> listarPageable(Pageable page) {
		return repo.findAll(page);
	}

}
