package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.dto.SignosDTO;
import com.mitocode.model.Signos;

public interface ISignosVitalesService extends ICRUD<Signos, Integer>{
	Signos registrarTransaccional(SignosDTO signos) throws Exception;
	
	Page<Signos> listarPageable(Pageable page);

}
