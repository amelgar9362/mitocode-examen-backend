package com.mitocode.repo;

import com.mitocode.model.Signos;

public interface ISignosVitalesRepo extends IGenericRepo<Signos, Integer> {

}
