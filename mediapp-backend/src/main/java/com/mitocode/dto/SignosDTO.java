package com.mitocode.dto;

import java.time.LocalDateTime;

import io.swagger.v3.oas.annotations.media.Schema;

public class SignosDTO {
	
	private Integer idSignos; 
	
	private String temperatura;	
	
	private String ritmo;
	
	private String pulso;
	
	private LocalDateTime fecha;
	
	private PacienteDTO paciente;
	public String getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}
	public String getRitmo() {
		return ritmo;
	}
	public void setRitmo(String ritmo) {
		this.ritmo = ritmo;
	}
	public String getPulso() {
		return pulso;
	}
	public void setPulso(String pulso) {
		this.pulso = pulso;
	}
	public LocalDateTime getFecha() {
		return fecha;
	}
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	
	public PacienteDTO getPaciente() {
		return paciente;
	}
	public void setPaciente(PacienteDTO paciente) {
		this.paciente = paciente;
	}
	public Integer getIdSignos() {
		return idSignos;
	}
	public void setIdSignos(Integer idSignos) {
		this.idSignos = idSignos;
	}
	
	
	
	

}
